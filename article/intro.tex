
\section{Introduction}
\label{sec:introduction}

The gaming industry is at a turning point of its history. The industry
had traditionally distinguished an handful of hardware companies
(including Sony, Microsoft, and Nintendo) and thousands of software
producers, ranging from start-ups to the big \emph{game studios} such
as Electronics Arts, Blizzard, and Ubisoft. The former companies sell
devices (typically game consoles) to the gamers while the latter ones
sell software that run on the dedicated devices. This landscape has
essentially remained unchanged for years\footnote{Note that some
    virtual companies (especially Steam, but also Apple AppStore and
    Google PlayStore) have replaced physical stores for the delivery
    of games to the gamers. But these new players do not essentially
changed the overall value chain of the gaming industry.} but many game
business analysts consider that this landscape has good chances to
dramatically change in the next few years. The key driver of this
mutation is what is now referred to as \emph{cloud gaming}.

We illustrate the fundamentals of cloud gaming in
Figure~\ref{fig:fundamentals}. The main idea is that the game engine,
which is the part of the game software in charge of rendering the
game, is deported ``in the cloud''. To be more precise, it runs on
commoditized servers in data-centers; it gets the gamer actions from
the Internet and it delivers the output video as a video stream, which
is also delivered through the Internet to the display device of the
gamer. \noteXC{The rendering in the cloud + video streaming to the
    client is main idea for cloud gaming but alternative vision exists
    where rendering instruction are streamed to the client or where
    game modules can run either on the cloud or on the client
    depending on the context (c.f.
    \cite{cai_gaming_2014,cai_cognitive_2015,semsarzadeh_video_2015})}

\begin{figure}[thb]
  \centering
  \tikzsetnextfilename{cloud-gaming-in-a-nutshell}
  \begin{tikzpicture}[xscale = 0.7, yscale=0.9]
    \def\xdc{7}
    \def\xInt{3}
    % the command
    \node at (0,-1.5) (command)
    {\pgfbox[center,center]{\pgfuseimage{gamer}}};
    \node (commandOut) [right=5pt of command] {};

    % the tv
    \node at (0,1.5) (tv)
    {\pgfbox[center,center]{\pgfuseimage{tv}}};
    \node (tvIn) [right=-1pt of tv] {};
    
    % the data-center
    \node[text width=2cm,rounded corners, draw,fill=gray!50, minimum
    height=1.5cm] (lsd) at (\xdc,0) {\vspace{1.9cm}};
    \node[draw=none,fill=none,anchor=north] at (lsd.north)
    {Data-Center}; 

    \foreach \pos in {-0.7,0.7}{ \node at (\xdc+\pos,-0.3)
      {\pgfbox[center,center]{\pgfuseimage{rack}}};}
    
    % the Internet
    \node[cloud,cloud puffs=11,fill=gray!20, minimum width=3.3cm, 
    minimum height=2.5cm] (cdn) at (\xInt,0) {~};
    \node at (cdn) {Internet};

    \draw[ultra thick, ->, gray] (commandOut) .. controls (cdn.south
    west) and (cdn.south east) ..
    node[font=\small,below,sloped] {user actions} (lsd);
    
    \draw[ultra thick, ->, gray] (lsd) .. controls (cdn.north
    east) and (cdn.north west) ..
    node[font=\small,above,sloped] {video stream} (tvIn);

  \end{tikzpicture}
  \caption{Cloud gaming in a Nutshell }
  \label{fig:fundamentals}
\end{figure}

The present paper provides a 2016's snapshot of the research axis that
have been explored by scientists so far, as well as some perspectives
about the points that, to our opinion, scientists should pay a special
attention at.

\subsection{Why Does Cloud Gaming Make Sense}
\label{sec:why-does-cloud}

Two categories of actors in the gaming value chain are pushing hard
for the development of cloud gaming: game developers and end-user
gamers.

\subsubsection*{Expected Improvements for Game Developers }
\label{sec:expect-impr-game}

The game developers have many reasons to believe that the development
of cloud gaming solutions will lead to both a reduction of cost and
new business opportunities for them.

The cost reduction is the consequence of having only one virtual
platform for hosting the game engine. In today's market, even though
the main game engines (\textit{e.g.} Unity) enable easy
cross-platform development, the game developers have to make specific
developments for the multiple hardware devices and operating systems
that may host the game. In a cloud gaming solution, the game engine
runs in a \ac{VM}, which runs in the data-center, regardless of the
underlying hardware capabilities. Since it is possible to accurately
define the characteristics of \acp{VM} and to decide, once and for
all, whether a given \ac{VM} can run on a specific hardware platform,
the task of the game programmers would be restricted to design one
version of their games for any kind of end-users.

The business opportunities for game developers are numerous. We
emphasize in the following three key opportunities. The first one is
the better piracy control that cloud gaming solutions provide to the
game studio.  With a game engine running entirely on the cloud, the
piracy is made far harder than today, where it is still possible to
make the home hardware run illegal copies of the games. The second one
is the opportunity to upgrade games. Not only the game studios can
modify the game play with regard to the analysis of gamer logs, but
also the release of new games can be made earlier with promises of
future game developments (for example new levels or new monsters). A
third opportunity is related to new ways of selling the game products.
Cloud gaming solutions allow game studios to develop
subscription-based offers based on a better control of how much gamers
actually play games.

\subsubsection*{Expected Improvements for Gamers}
\label{sec:expect-impr-gamers}

Gamers have also many reasons to believe that cloud gaming will be a
progress for them. The most obvious reason is the independency to
hardware platforms. For most gamers, the choice of the console to buy
or of the hardware graphical component to add to the PC is Cornelian.
Some gamers make unfortunate choices (people buying a hardware device
that appears to become a flop, with no game studios developing games
for this hardware platform). With cloud gaming solutions, the gamers
only focus on the games to play, and nothing about hardware
characteristics (even if the cloud gaming platforms will eventually
have to differentiate with offers that will probably be compared in a
similar way as today's game consoles are).

More generally, cloud gaming bring to gamers all the positive aspects
of cloud services, especially the development of on-demand offers, an
ubiquitous access to a large catalog of games, and no maintenance
operations. A typical use case enabled by cloud gaming is a
\emph{follow-me} service, where the gamer keeps on playing while in
mobility, switching from her TV device at home to her mobile device in
a public transportation. Gaming in public area such as airports is
also considered as an opportunity that cloud gaming can provide.

\subsection{Why is Cloud Gaming So Challenging}
\label{sec:why-cloud-gaming}

Despite all the previously described attractive features, the game
industry is not ready to shift to cloud gaming yet. The path to a
large-scale adoption of cloud gaming has indeed revealed to be more
hazardous than expected so far. A spectacular demonstration of the
difficulties was the bankruptcy of one of the cloud gaming pioneers
named OnLive. As we will see all along this survey paper, the
challenges are numerous. We identify two main families of challenges:
the \ac{QoE} for the end-users, and the infrastructure costs for the
cloud gaming platforms.

The \ac{QoE} for users depends mostly on two factors: the quality of
the video and the responsiveness of the games to the actions. The
latter factor has become a big concern for scientists and most of the
research papers on cloud gaming have aimed at studying whether it is
actually possible to runs a game on a distant site in the Internet
while maintaining a response time in an acceptable range for the
users. The answer to this question being negative for a vast
population of gamers, the scientists have started what we call a
\emph{millisecond chase}, trying to study solutions to gain a few
milliseconds (ms) on every stage of the cloud gaming chain.

The infrastructure costs are mainly related to the reservation of
computing facilities for the game engines. In other cloud services,
sharing the hardware in data-centers among several services and
multiple users have allowed the development of so-called elastic
services. In addition the consolidation of the virtual software in the
data-center has resulted in massive savings for the service provider.
Unfortunately, game engines are much more complicated software, which
does not accommodate easily of virtualization on commoditized
computers. The difficulties to run several game engines instance on a
given server are likely to prevent cloud gaming services to scale at a
reasonable price. Some scientific efforts have also focused on this
critical topic.

\subsection{Paper Organization}
\label{sec:paper-organization}

This paper reviews the recent works on cloud gaming. Many papers have
been published in the area and not all of them,% address concerns that,
in our opinion, are critical for cloud gaming. We thus admit that we
do not cover \emph{all} papers related to cloud gaming but only the
ones that have generated some kind of excitement at their reading. In
other words, this paper is not an exhaustive survey, but a hopefully
interesting mix of a survey and a position paper, which is expected to
inspire future research works on this area.

In Section~\ref{sec:glob-descr-latency}, we describe the overall chain
for cloud gaming. We provide here a modular description of cloud
gaming and we will emphasize that each of this module is responsible
of a few additional time delays.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
